# TBBconverter 

The TBBconverter converts a TBB raw file into a series of UDP packets that can are stored into a binary file.

Such file can be used as an input for the correlator.
## Requirements for compilation

To compile the software you need the GSL (GNU Scientific Libraries) libraries and the HDF5 ones.

### ON DAS6

If you have access on DAS6 you are in luck as you can use the module program to request the right libraries. In particular, 
these are the modules to add:

```bash 
module load spack
module load hdf5 gsl
```

## USAGE

To compile you can execute the following commands

```sh
mkdir build 
cd build
cmake ..
make
```

This will produce a binary file inside build called `convertTBB`

To execute run 
```bash
convertTBB [tbbh5filesinglestation].h5 [outputfile] 
```

To test a small dataset for one station and only 10 timesamples is available in the directory`test_data`
