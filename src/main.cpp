#include <iostream>

#include <H5Cpp.h>
#include <map>
#include <gsl/gsl_fft_real_float.h>

#include <alloca.h>
#include <cassert>
#include <complex>
#include "weights.hpp"
#include <argh.h>
#include <cstring>

using namespace H5;

const int NR_DIPOLES_PER_STATION = 48 * 2;
const int NR_SUBBANDS = 512;
const int FFT_SIZE = 2 * NR_SUBBANDS;
const int NR_SUBBANDS_PER_BUNDLE = 8;
const int NR_BUNDLES = NR_SUBBANDS / NR_SUBBANDS_PER_BUNDLE;
const int NR_TIMES_PER_PACKET = 2;
const int FFT_CHANNELS = 512;
const int NR_TAPS = 16;
const double SAMPLE_RATE = 200'000'000;
const double SUBBAND_BANDWIDTH = SAMPLE_RATE / FFT_SIZE;

// CONVERT TIMESTAMP Timestamp is number of seconds since January 1st, 1970 UTC multiplied by 195312.5 which corresponds to 5.12 us

typedef struct Packet {
    struct Header {
        uint64_t rsp_lane_id: 2;
        uint64_t rsp_sdo_mode: 2;
        uint64_t rsp_rsp_clock: 1;
        uint64_t rsp_reserved_1: 59;

        uint16_t rsp_station_id;
        uint16_t nof_words_per_block;
        uint16_t nof_blocks_per_packet;

        uint64_t rsp_bsn /*: 50;
      uint64_t rsp_reserved_0 : 13;
      uint64_t rsp_sync : 1*/;
    } __attribute__((packed)) header;

    std::complex <int16_t> payload[NR_TIMES_PER_PACKET][NR_SUBBANDS_PER_BUNDLE][NR_DIPOLES_PER_STATION];
} UDPPacket;

template<typename T>
T bswap(const T &v) {
    static_assert(sizeof(T) == 1 || sizeof(T) == 2 || sizeof(T) == 4 || sizeof(T) == 8);

    switch (sizeof(T)) {
        case 1 :
            return v;
        case 2 :
            return __bswap_16(v);
        case 4 :
            return __bswap_32(v);
        case 8 :
            return __bswap_64(v);
    }
}

template<typename T>
T little_endian(const T &v) {
#if defined __BIG_ENDIAN__
    return bswap(v);
#else
    return v;
#endif
}

template<typename T>
T big_endian(const T &v) {
#if defined __BIG_ENDIAN__
    return v;
#else
    return bswap(v);
#endif
}

void applyWeights(float fft_input[FFT_SIZE], const int16_t history[NR_TAPS][FFT_SIZE], unsigned start_index) {
    for (unsigned sample = 0; sample < FFT_SIZE; sample++) {
#if 1
        int sum = 0;

        for (unsigned tap = 0; tap < NR_TAPS; tap++)
            sum += originalStationPPFWeights[sample][tap] * history[(start_index - tap) % NR_TAPS][sample];

        fft_input[sample] = sum;
#else
        fft_input[sample] = history[start_index][sample];
#endif
    }
}

//void readSample(gsl_complex_packed_array_float fft_output, uint64_t sample_id, int16_t history[NR_TAPS][FFT_SIZE],
void readSample(float fft_output[FFT_SIZE], uint64_t sample_id, int16_t history[NR_TAPS][FFT_SIZE],
                H5::DataSet *dset) {
    float fft_data[FFT_SIZE];
    hsize_t dims[] = {FFT_SIZE};
    hsize_t offset[] = {sample_id * FFT_SIZE};   // hyperslab offset in the file
    hsize_t count[] = {FFT_SIZE};    // size of the hyperslab in the file
    DataSpace memspace(1, dims);
    DataSpace dataspace = dset->getSpace();
    dataspace.selectHyperslab(H5S_SELECT_SET, count, offset);
    dset->read(history[sample_id % NR_TAPS], PredType::NATIVE_INT16, memspace, dataspace);
    applyWeights(fft_data, history, sample_id % NR_TAPS);
    gsl_fft_real_float_radix2_transform(fft_data, 1, FFT_SIZE);

    for (unsigned subband = 0; subband < NR_SUBBANDS; subband ++) {
      fft_output[2 * subband    ] = fft_data[subband];
      fft_output[2 * subband + 1] = fft_data[FFT_SIZE - 1 - subband];
    }
}

void zeroSample(gsl_complex_packed_array_float fft_input, uint64_t sample_id, int16_t history[NR_TAPS][FFT_SIZE]) {
    std::memset(history[sample_id % NR_TAPS], 0, FFT_SIZE * sizeof(int16_t));
    std::memset(fft_input, 0, FFT_SIZE * sizeof(float));
}

uint64_t getStartTime(const DataSet &dset) {
    int refTime;
    auto refTimeAttr = Attribute(dset.openAttribute("TIME"));
    refTimeAttr.read(refTimeAttr.getDataType(), &refTime);
    std::cout << "Reftime is " << refTime << std::endl;
    return refTime * SUBBAND_BANDWIDTH;
}

uint64_t getTSamples(const DataSet &dset) {
    hsize_t size[1];
    dset.getSpace().getSimpleExtentDims(size);
    std::cout << "# of samples is " << size[0] << std::endl;
    return size[0] / FFT_SIZE;
}

void print_help() {
    std::cout << "convertTBB - Utility to convert TBB data into raw UDP packets that can be fed to a LOFAR correlator"
              << std::endl;

    std::cout << "Usage: " << "convertTBB [datasetsPerStation ....]" << std::endl;
    std::cout << "or convertTBB --station-group [stationGroupName] --output-format [outputTemplate] [datasetsPerStation ....]" << std::endl;
    std::cout << "or convertTBB -sg [stationGroupName] -o [outputTemplate] [datasetsPerStation ....]" << std::endl;

}

int main(int argc, char *argv[]) {
    argh::parser parser;
    parser.add_param({"-sg", "--station-group", "-h", "--help"});
    parser.add_param({"-o", "--output-format"});
    parser.parse(argc - 1, &argv[1]);
    const auto positional_arguments = parser.pos_args();
    if (positional_arguments.empty() || parser["help"]) {
        print_help();
        exit(0);
    }

    std::vector <H5File> files;

    for (auto &fname: positional_arguments) {
        std::cout << "Opening file " << fname << std::endl;
        files.push_back(H5File(fname, H5F_ACC_RDONLY));
    }


    auto stationGroupName = files[0].getObjnameByIdx(0);
    std::string outputTemplate;
    parser({"-sg", "station-group"}, stationGroupName) >> stationGroupName;
    parser({"-o", "output-template"}, stationGroupName) >> outputTemplate;

    std::cout << "Opening station group with name " << stationGroupName << std::endl;
    std::map<unsigned, DataSet> dsets;

    for (auto &file: files) {
        auto stationGroup = file.openGroup(stationGroupName);

        std::cout << "Number of groups in " << file.getFileName() << ' ' << stationGroup.getId() << ' '
                  << stationGroup.getNumObjs()
                  << std::endl;

        for (short i = 0; i < stationGroup.getNumObjs(); i++) {
            std::cout << "Opening dset " << stationGroup.getObjnameByIdx(i) << std::endl;
            auto dset = stationGroup.openDataSet(stationGroup.getObjnameByIdx(i));
            int rcu_id;
            auto attribute = dset.openAttribute("RCU_ID");
            attribute.read(attribute.getDataType(), &rcu_id);
            dsets[rcu_id] = dset;
        }
    }
    std::cout << "Found dipoles:" << "\n";
    for (auto item: dsets) {
        std::cout << item.first << "\t";
        if ((item.first + 1) % 8 == 0) std::cout << "\n";
    }
    std::cout << std::endl;

    //READ ONE SAMPLE
    auto dtype = dsets[0].getTypeClass();
    switch (dtype) {
        case H5T_FLOAT:
            std::cout << "Datatype of dataset is " << "FLOAT" << std::endl;
            break;
        case H5T_INTEGER:
            std::cout << "Datatype of dataset is " << "INTEGER" << std::endl;
            break;
        default:
            std::cerr << "Datatype is neither integer nor float " << dtype << std::endl;
            exit(1);
    }

    std::vector < FILE * > fps(NR_BUNDLES);

    for (int bundle = 7; bundle < NR_BUNDLES - 7; bundle++) {
        char *filename = (char *) alloca(outputTemplate.size() + 16);
        sprintf(filename, "%s-sb%03u.raw", outputTemplate.c_str(), bundle * NR_SUBBANDS_PER_BUNDLE);
        fps[bundle] = fopen(filename, "wb");
    }

    const uint64_t start_time = getStartTime(dsets[0]);
    const uint64_t NR_T_SAMPLES = getTSamples(dsets[0]);
    std::map<unsigned, uint64_t> samples_per_dset;

    for (auto &item: dsets) {
        samples_per_dset[item.first] = getTSamples(item.second);
    }

    UDPPacket packets[NR_BUNDLES];
    float sample[FFT_SIZE];
    int16_t history[NR_DIPOLES_PER_STATION][NR_TAPS][FFT_SIZE];

    for (uint64_t time_major = 0; time_major < NR_T_SAMPLES / NR_TIMES_PER_PACKET *
                                               NR_TIMES_PER_PACKET /* round down */; time_major += NR_TIMES_PER_PACKET) {
        if (time_major % 1024U == 0)
            std::cout << "Converting time " << time_major << " of " << NR_T_SAMPLES << std::endl;

        for (uint64_t time_minor = 0; time_minor < NR_TIMES_PER_PACKET; time_minor++) {
            uint64_t time = time_major + time_minor;

            for (unsigned dipole_idx = 0; dipole_idx < NR_DIPOLES_PER_STATION; dipole_idx++) {
                if (samples_per_dset.find(dipole_idx) == samples_per_dset.end() ||
                    (time_major + NR_TIMES_PER_PACKET) >= samples_per_dset[dipole_idx]) {
                    zeroSample(sample, time, history[dipole_idx]);
                } else {
                    readSample(sample, time, history[dipole_idx], &dsets[dipole_idx]);
                }

                for (int subband_idx = 7 * NR_SUBBANDS_PER_BUNDLE;
                     subband_idx < NR_SUBBANDS - 7 * NR_SUBBANDS_PER_BUNDLE; subband_idx++) {
                    packets[subband_idx / NR_SUBBANDS_PER_BUNDLE].payload[time_minor][subband_idx %
                                                                                      NR_SUBBANDS_PER_BUNDLE][dipole_idx] =
                            std::complex<int16_t>(little_endian((int16_t)(sample[subband_idx * 2] / 1024)),
                                                  little_endian((int16_t)(sample[subband_idx * 2 + 1] / 1024)));
                }
            }
        }

        for (int bundle = 7; bundle < NR_BUNDLES - 7; bundle++) {
            packets[bundle].header.rsp_bsn = big_endian(start_time + time_major);
            fwrite(reinterpret_cast<void *>(&packets[bundle]), sizeof(UDPPacket), 1, fps[bundle]);
        }
    }

    for (int bundle = 7; bundle < NR_BUNDLES - 7; bundle++)
        fclose(fps[bundle]);
}
